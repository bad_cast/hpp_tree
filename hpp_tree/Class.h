#pragma once
#include "common.h"
#include "Parent.h"
#include "Method.h"
#include "Param.h"

namespace hpp_tree {
	struct Class {
		//uint32_t id;
		string name;
		map<string, Parent> m_parents;
		map<string, Param> m_params;
		//map<uint32_t, Method> m_methods;	//	todo: this map is for later version with overloaded methods
		map<string, Method> m_methods;
		Class() : name("") {};
		bool add_parent(const Parent& parent);
		bool remove_parent(const string& name);
		bool add_method(const Method& method);
		bool remove_method(const string& method_name);
		bool add_param(const Param& param);
		bool remove_param(const string& param_name);
	};
}
