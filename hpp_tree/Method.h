#pragma once
#include "common.h"
#include "Param.h"

namespace hpp_tree {
	struct Method {
		string name;				//	name of method
		map<string, Param> m_params;//	map of params of method
		ModVirtual mod_virtual;		//	virtual/override/final modifier
		ModConst mod_const;			//	const modifier
		ModAccess mod_access;		//	access modifier
		Type ret_type;				//	return Type
		string ret_class_name;		//	return class name
		int8_t ret_ptr_ref;			//	return pointer or reference (-1 is reference, >=1 are pointers, 0 is value)
		
		Method() : name(""), mod_virtual(V_NONE), mod_const(C_NONE), mod_access(A_PUBLIC), 
			ret_type(T_VOID), ret_class_name(""), ret_ptr_ref(0) {}
		bool add_param(const Param& param);		//	add param to method
		bool remove_param(const string& name);	//	remove param from method
	};
}