#pragma once

#include <map>
#include <string>
#include <vector>
#include "boost//property_tree//ptree.hpp"
#include "boost//property_tree//xml_parser.hpp"
#include <boost//foreach.hpp>

using namespace std;
namespace hpp_tree {
	enum Type { T_VOID, T_INT, T_FLOAT, T_DOUBLE, T_CHAR, T_ENUM, T_STRUCT, T_CLASS };
	enum ModVirtual { V_NONE, V_VIRTUAL, V_OVERRIDE, V_FINAL };
	enum ModConst { C_NONE, C_CONST };
	enum ModAccess { A_PUBLIC, A_PRIVATE, A_PROTECTED };
}

