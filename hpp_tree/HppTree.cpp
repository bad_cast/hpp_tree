#include "pch.h"
#include "HppTree.h"
#include <iostream>

namespace pt = boost::property_tree;

HPPTREE_API void hpp_tree::clear(void) {
	m_classes.clear();
}

HPPTREE_API bool hpp_tree::load_from_xml(const string& filename) {
	pt::ptree tree;
	try {
		pt::read_xml(filename, tree, pt::xml_parser::trim_whitespace);
	}
	catch (...) {
		cout << "Can't read " << filename << "." << endl;
		return false;
	}

	m_classes.clear();
	BOOST_FOREACH(auto& v, tree.get_child("classes")) {
		Class cls;
		pt::ptree parents, methods, params;
		cls.name = v.second.get<std::string>("name");
		try { 
			parents = v.second.get_child("parents"); 
		}
		catch (...) { ; }
		try {
			methods = v.second.get_child("methods");
		}
		catch (...) { ; }
		try {
			params = v.second.get_child("params");
		}
		catch (...) { ; }
		BOOST_FOREACH(auto& p, parents) {
			Parent cls_par;
			//cls_par.class_id = p.second.get<uint32_t>("class_id");
			cls_par.mod_virtual = (ModVirtual)p.second.get<uint8_t>("mod_virtual");
			cls_par.mod_access = (ModAccess)p.second.get<uint8_t>("mod_access");
			cls.m_parents[cls_par.name] = cls_par;
		}
		BOOST_FOREACH(auto& m, methods) {
			Method cls_method;
			pt::ptree method_params;
			cls_method.name = m.second.get<string>("name");
			cls_method.mod_virtual = (ModVirtual)m.second.get<uint8_t>("mod_virtual");
			cls_method.mod_access = (ModAccess)m.second.get<uint8_t>("mod_access");
			cls_method.mod_const = (ModConst)m.second.get<uint8_t>("mod_const");
			cls_method.ret_type = (Type)m.second.get<uint8_t>("ret_type");
			cls_method.ret_class_name = m.second.get<string>("ret_class_name");
			cls_method.ret_ptr_ref = m.second.get<int8_t>("ret_ptr_ref");
			try { method_params = m.second.get_child("params"); }
			catch (...) { ; }
			BOOST_FOREACH(auto& p, method_params) {
				Param param;
				param.name = p.second.get<string>("name");
				param.type = (Type)p.second.get<uint8_t>("type");
				param.class_name = p.second.get<string>("class_name");
				param.mod_const = (ModConst)p.second.get<uint8_t>("mod_const");
				param.ptr_ref = p.second.get<int8_t>("ptr_ref");
				param.size = p.second.get<uint32_t>("size");
				cls_method.m_params[param.name] = param;
			}
			cls.m_methods[cls_method.name] = cls_method;
		}
		BOOST_FOREACH(auto& p, params) {
			Param param;
			param.name = p.second.get<string>("name");
			param.class_name = p.second.get<string>("class_name");
			param.mod_const = (ModConst)p.second.get<uint8_t>("mod_const");
			param.mod_access = (ModAccess)p.second.get<uint8_t>("mod_access");
			param.type = (Type)p.second.get<uint8_t>("type");
			param.ptr_ref = p.second.get<int8_t>("ptr_ref");
			param.size = p.second.get<uint32_t>("size");
			cls.m_params[param.name] = param;
		}
		m_classes[cls.name] = cls;
	}

	return true;
}

HPPTREE_API bool hpp_tree::save_to_xml(const string& filename) {
	pt::ptree t;
	auto & t_cs = t.add("classes", "");
	BOOST_FOREACH(const auto& cls, m_classes) {
		auto & c = t_cs.add("class","");
		c.put("name", cls.second.name);
		auto & t_pts = c.add("parents", "");
		auto & t_ms = c.add("methods", "");
		auto & t_pms = c.add("params", "");
		BOOST_FOREACH(const auto& par, cls.second.m_parents) {
			auto & pt = t_pts.add("parent", "");
			pt.put("name", par.second.name);
			pt.put("mod_virtual", par.second.mod_virtual);
			pt.put("mod_access", par.second.mod_access);
		}
		BOOST_FOREACH(const auto& method, cls.second.m_methods) {
			auto & mt = t_ms.add("method", "");
			mt.put("name", method.second.name);
			mt.put("mod_access", method.second.mod_access);
			mt.put("mod_virtual", method.second.mod_virtual);
			mt.put("mod const", method.second.mod_const);
			mt.put("ret_type", method.second.ret_type);
			mt.put("ret_class_name", method.second.ret_class_name);
			mt.put("ret_ptr_ref", method.second.ret_ptr_ref);
			auto & t_m_pms = t_ms.add("params", "");
			BOOST_FOREACH(const auto& param, method.second.m_params) {
				auto & t_m_pm = t_m_pms.add("param", "");
				t_m_pm.put("name", param.second.name);
				t_m_pm.put("mod_const", param.second.mod_const);
				t_m_pm.put("type", param.second.type);
				t_m_pm.put("ptr_ref", param.second.ptr_ref);
				t_m_pm.put("size", param.second.size);
				t_m_pm.put("class_name", param.second.class_name);
			}
		}
		BOOST_FOREACH(const auto& par, cls.second.m_params) {
			auto & t_pm = t_pms.add("param", "");
			t_pm.put("name", par.second.name);
			t_pm.put("mod_access", par.second.mod_access);
			t_pm.put("mod_const", par.second.mod_const);
			t_pm.put("type", par.second.type);
			t_pm.put("ptr_ref", par.second.ptr_ref);
			t_pm.put("size", par.second.size);
			t_pm.put("class_name", par.second.class_name);
		}
	}
	
	try {
		pt::write_xml(filename, t, std::locale(), pt::xml_writer_make_settings<pt::ptree::key_type>('\t', 1u));
	}
	catch (...) {
		cout << "Writing to " << filename << " failed.";
		return false;
	}
	return true;
}


//HPPTREE_API uint32_t hpp_tree::inc_last_id(void) {
//	return ++hpp_tree::last_id;
//}

HPPTREE_API bool hpp_tree::add_class(Class& cls) {
	if (m_classes.count(cls.name)) {
		return false;
	}
	//cls.id = inc_last_id();
	m_classes[cls.name] = cls;
	return true;
}

HPPTREE_API bool hpp_tree::remove_class(const string& class_name) {
	//	return false if other classes have this class as parent 
	BOOST_FOREACH(const auto & c, m_classes) {
		if(c.second.m_parents.count(class_name)){
			return false;
		}
	}
	if (!m_classes.erase(class_name)) {
		return false;
	}
	return true;
}

HPPTREE_API bool hpp_tree::add_parent(const string& class_name, const Parent& parent)
{
	if(!m_classes.count(class_name)) {
		return false;
	}
	return m_classes[class_name].add_parent(parent);
}

HPPTREE_API bool hpp_tree::remove_parent(const string& class_name, const string& parent_name) {
	if (!m_classes.count(class_name)) {
		return false;
	}
	return m_classes[class_name].remove_parent(parent_name);
}

HPPTREE_API bool hpp_tree::add_method(const string& class_name, const Method& method) {
	if (!m_classes.count(class_name)) {
		return false;
	}
	return m_classes[class_name].add_method(method);
}

HPPTREE_API bool hpp_tree::remove_method(const string& class_name, const string& method_name) {
	if (!m_classes.count(class_name)) {
		return false;
	}
	return m_classes[class_name].remove_method(method_name);
}

HPPTREE_API bool hpp_tree::add_param(const string& class_name, const Param& param)
{
	if (!m_classes.count(class_name)) {
		return false;
	}
	return m_classes[class_name].add_param(param);
}

HPPTREE_API bool hpp_tree::remove_param(const string& class_name, const string& param_name)
{
	if (!m_classes.count(class_name)) {
		return false;
	}
	return m_classes[class_name].remove_param(param_name);
}

HPPTREE_API bool hpp_tree::add_method_param(const string& class_name, const string& method_name, const Param& param)
{
	if (!m_classes.count(class_name)) {
		return false;
	}
	return !m_classes[class_name].m_methods.count(method_name) ? false : 
		m_classes[class_name].m_methods[method_name].add_param(param);
}

HPPTREE_API bool hpp_tree::remove_method_param(const string& class_name, const string& method_name, const string& param_name)
{
	if (!m_classes.count(class_name)) {
		return false;
	}
	return !m_classes[class_name].m_methods.count(method_name) ? false : 
		m_classes[class_name].m_methods[method_name].remove_param(param_name);
}

HPPTREE_API bool hpp_tree::get_class(const string& class_name, Class& cls) {
	if (!m_classes.count(class_name)){
		return false;
	}
	cls = m_classes[class_name];
	return true;
}