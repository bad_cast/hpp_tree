#include "pch.h"
#include "Class.h"

bool hpp_tree::Class::add_parent(const Parent& parent) {
	if (!m_parents.count(parent.name)) {
		m_parents[parent.name] = parent;
		return true;
	}
	return false;
}

bool hpp_tree::Class::remove_parent(const string& name) {
	if (m_parents.erase(name)) {
		return true;
	}
	return false;
}

bool hpp_tree::Class::add_method(const Method& method) {
	// todo: methods can be overloaded, not supported now
	if (!m_methods.count(method.name)) {
		m_methods[method.name] = method;
		return true;
	}
	return false;
}

bool hpp_tree::Class::remove_method(const string& method_name) {
	if (m_methods.erase(method_name)) {
		return true;
	}
	return false;
}

bool hpp_tree::Class::add_param(const Param& param) {
	if (m_params.count(param.name)) {
		return false;
	}
	m_params[param.name] = param;
	return true;
}

bool hpp_tree::Class::remove_param(const string& param_name) {
	if (!m_params.erase(param_name)) {
		return false;
	}
	return true;
}


