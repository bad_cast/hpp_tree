#pragma once
#include "common.h"

namespace hpp_tree {
	struct Param {
		string name;			//	name of param/data
		Type type;				//	Type
		string class_name;		//	class id, if type is T_CLASS
		ModConst mod_const;		//	const modifier
		ModAccess mod_access;	//	access modifier
		int8_t ptr_ref;			//	pointer or reference (-1 is reference, >=1 are pointers, 0 is value)
		uint32_t size;			//	size of array (if not array size = 0)
		Param() : name(""), type(T_VOID), class_name(""), mod_const(C_NONE), mod_access(A_PUBLIC), ptr_ref(0), size(0) {}
	};
}