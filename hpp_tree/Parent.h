#pragma once
#include "common.h"

namespace hpp_tree {
	struct Parent {
		string name;			//	name of parent
		ModVirtual mod_virtual;	//	virtual modifier
		ModAccess mod_access;	//	access modifier
		Parent() : mod_access(A_PUBLIC), mod_virtual(V_NONE) {};
	};
}