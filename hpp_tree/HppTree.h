#pragma once
#include "common.h"
#include "Class.h"

#ifdef HPPTREE_EXPORTS
#define HPPTREE_API __declspec(dllexport)
#else
#define HPPTREE_API __declspec(dllimport)
#endif

//- ����������� ������������
//- ���������
//
//������� ������� ������ ����
//- �����
//- ��������� �� �����
//- �������
//- ���������� �������
//- ��������� �� ���������� �������
//- ������ �� ���������� �������
//- ��������� �� ������ � ����������� ���������

namespace hpp_tree {
	static map<string, Class> m_classes;
	//static int last_id = 0;

	//  exported functions
	extern "C" HPPTREE_API void clear(void);
	extern "C" HPPTREE_API bool load_from_xml(const string& filename);
	extern "C" HPPTREE_API bool save_to_xml(const string& filename);
	extern "C" HPPTREE_API bool add_class(Class& cls);
	extern "C" HPPTREE_API bool remove_class(const string& class_name);
	extern "C" HPPTREE_API bool add_parent(const string& class_name, const Parent& parent);
	extern "C" HPPTREE_API bool remove_parent(const string& class_name, const string& parent_name);
	extern "C" HPPTREE_API bool add_method(const string& class_name, const Method& method);
	extern "C" HPPTREE_API bool remove_method(const string& class_name, const string& method_name);
	extern "C" HPPTREE_API bool add_param(const string& class_name, const Param& param);
	extern "C" HPPTREE_API bool remove_param(const string& class_name, const string& param_name);
	extern "C" HPPTREE_API bool add_method_param(const string& class_name, const string& method_name, const Param& param);
	extern "C" HPPTREE_API bool remove_method_param(const string& class_name, const string& method_name, const string& param_name);
	extern "C" HPPTREE_API bool get_class(const string& class_name, Class& cls);

	//	local functions
	//uint32_t inc_last_id(void);
}

