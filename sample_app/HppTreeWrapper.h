#pragma once

#include <string>
#include <windows.h>
#include "HppTree.h"

using namespace std;
using namespace hpp_tree;

typedef bool  (*PROC_LOAD)(const string& filename);
typedef bool  (*PROC_SAVE)(const string& filename);
typedef bool  (*PROC_ADD_CLASS)(Class& cls);
typedef bool  (*PROC_REMOVE_CLASS)(const string& class_name);
typedef bool  (*PROC_ADD_PARENT)(const string& class_name, const Parent& parent);
typedef bool  (*PROC_REMOVE_PARENT)(const string& class_name, const string& parent_name);
typedef bool  (*PROC_ADD_METHOD)(const string& class_name, const Method& method);
typedef bool  (*PROC_REMOVE_METHOD)(const string& class_name, const string& method_name);
typedef bool  (*PROC_ADD_PARAM)(const string& class_name, const Param& param);
typedef bool  (*PROC_REMOVE_PARAM)(const string& class_name, const string& param_name);
typedef bool  (*PROC_ADD_METHOD_PARAM)(const string& class_name, const string& method_name, const Param& param);
typedef bool  (*PROC_REMOVE_METHOD_PARAM)(const string& class_name, const string& method_name, const string& param_name);
typedef bool  (*PROC_GET_CLASS)(const string& class_name, Class& cls);


class HppTreeWrapper
{
	HMODULE h_lib;
public:
	HppTreeWrapper();
	~HppTreeWrapper();
	bool init(string dllpath="hpp_tree.dll");
	void unload_lib();

	PROC_LOAD load_from_xml;
	PROC_SAVE save_to_xml;
	PROC_ADD_CLASS add_class;
	PROC_REMOVE_CLASS remove_class;
	PROC_ADD_PARENT add_parent;
	PROC_REMOVE_PARENT remove_parent;
	PROC_ADD_METHOD add_method;
	PROC_REMOVE_METHOD remove_method;
	PROC_ADD_PARAM add_param;
	PROC_REMOVE_PARAM remove_param;
	PROC_ADD_METHOD_PARAM add_method_param;
	PROC_REMOVE_METHOD_PARAM remove_method_param;
	PROC_GET_CLASS get_class;
};
