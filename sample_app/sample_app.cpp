// sample_app.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "HppTreeWrapper.h"

using namespace std;
using namespace hpp_tree;

int main()
{
	HppTreeWrapper tw;
	if (!tw.init()) {
		cout << "Faild to load library." << endl;
		exit(0);
	}
	
	tw.load_from_xml("test.xml");
	{
		// fill class info
		Class cls;
		cls.name = "C";
		// fill parent info
		hpp_tree::Parent parent;
		parent.name = "B";
		parent.mod_access = hpp_tree::A_PRIVATE;
		parent.mod_virtual = hpp_tree::V_FINAL;
		cls.add_parent(parent);
		// fill methods
		hpp_tree::Method method;
		method.name = "Validate";
		method.mod_const = hpp_tree::C_CONST;
		method.mod_access = hpp_tree::A_PROTECTED;
		method.mod_virtual = hpp_tree::V_FINAL;
		method.ret_type = hpp_tree::T_INT;
		method.ret_ptr_ref = -1;
		// fill method data
		hpp_tree::Param m_param;
		m_param.name = "cls";
		m_param.class_name = "A";
		m_param.mod_const = hpp_tree::C_CONST;
		m_param.ptr_ref = 1;
		m_param.size = 0;
		m_param.type = hpp_tree::T_CLASS;
		method.add_param(m_param);
		// fill class data
		hpp_tree::Param d;
		d.name = "A_ptr";
		d.mod_access = hpp_tree::A_PRIVATE;
		d.mod_const = hpp_tree::C_CONST;
		d.type = hpp_tree::T_CLASS;
		d.size = 0;
		d.ptr_ref = 1;
		cls.add_param(d);
		// add class
		tw.add_class(cls);
	}
	
	// save to xml
	tw.save_to_xml("test2.xml");
	return 0;
}
