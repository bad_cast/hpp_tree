#include "HppTreeWrapper.h"

HppTreeWrapper::HppTreeWrapper() :  h_lib(NULL),
	load_from_xml (nullptr),
	save_to_xml (nullptr),
	add_class (nullptr),
	remove_class (nullptr),
	add_parent (nullptr),
	remove_parent (nullptr),
	add_method (nullptr),
	remove_method (nullptr),
	add_param (nullptr),
	remove_param (nullptr),
	add_method_param (nullptr),
	remove_method_param (nullptr),
	get_class (nullptr) {
}

HppTreeWrapper::~HppTreeWrapper() {
	unload_lib();
}

bool HppTreeWrapper::init(string dllpath) {
	unload_lib();
	h_lib = ::LoadLibrary(dllpath.c_str());
	if (!h_lib)
		return false;
	load_from_xml = (PROC_LOAD)::GetProcAddress(h_lib, "load_from_xml");
	save_to_xml = (PROC_SAVE)::GetProcAddress(h_lib, "save_to_xml");
	add_class = (PROC_ADD_CLASS)::GetProcAddress(h_lib, "add_class");
	remove_class = (PROC_REMOVE_CLASS)::GetProcAddress(h_lib, "remove_class");
	add_parent = (PROC_ADD_PARENT)::GetProcAddress(h_lib, "add_parent");
	remove_parent = (PROC_REMOVE_PARENT)::GetProcAddress(h_lib, "remove_parent");
	add_method = (PROC_ADD_METHOD)::GetProcAddress(h_lib, "add_method");
	remove_method = (PROC_REMOVE_METHOD)::GetProcAddress(h_lib, "remove_method");
	add_param = (PROC_ADD_PARAM)::GetProcAddress(h_lib, "add_param");
	remove_param = (PROC_REMOVE_PARAM)::GetProcAddress(h_lib, "remove_param");
	add_method_param = (PROC_ADD_METHOD_PARAM)::GetProcAddress(h_lib, "add_method_param");
	remove_method_param = (PROC_REMOVE_METHOD_PARAM)::GetProcAddress(h_lib, "remove_method_param");
	get_class = (PROC_GET_CLASS)::GetProcAddress(h_lib, "get_class");
	return true;
}

void HppTreeWrapper::unload_lib() {
	if (h_lib) {
		::FreeLibrary(h_lib);
		h_lib = NULL;
	}
}
