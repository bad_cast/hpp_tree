### Info
- **hpp_tree** - library for saving and loading c++ class descriptions
- **sample_app** - sample application to work with hpp_tree library  

### Some notes.  
- Library is not considering overloaded methods  
- hpp_tree is not a tree, but map of trees. Each tree is class description  
- errors processing should be done through exceptions, not just return bool